<?php // $Id$ 
/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
?>
<div class="node <?php print $node_classes; ?>" id="node-<?php print $node->nid; ?>">
  <div class="node-inner inner">

		<?php if ($page == 0): ?>	
			<h2 class="node-title"><a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a></h2>
		<?php endif; ?>

		<?php if ($unpublished): ?>
			<div class="unpublished"><?php print t('Unpublished'); ?></div>
		<?php endif; ?>

		<?php if ($submitted && $page == 1): ?>
			<div class="submitted">
				<abbr class="date" title="<?php print $long_date; ?>"> <?php print $short_date; ?></abbr>
				<span class="author"><?php print t(' by ') . $name; ?></span>
			</div>
		<?php endif; ?>

		<?php if (!empty($picture) && ($node->type != 'poll')): ?>
			<div class="picture">
				<?php print $picture; ?>
			</div>
		<?php endif; ?>

  <div class="node-content">
		  <?php print $content; ?>
  </div>

		<?php if (count($taxonomy) && $page == 1): ?>
			<div class="tags"><?php print $terms; ?></div>
		<?php endif; ?>

		<?php if ($links): ?>
			<div class="actions"><?php print $links; ?></div>
		<?php endif; ?>

  </div>
</div> <!-- /node -->